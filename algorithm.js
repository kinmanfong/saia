//populate insulin type selection
var insulin = [
  {Name:"Humulin R",Type:"R"},
  {Name:"Humulin N",Type:"B"},
  {Name:"Humulin 30/70",Type:"M"},
  {Name:"Actrapid (Novopen)",Type:"R"},
  {Name:"Insulatard (Novopen)",Type:"B"},
  {Name:"Mixtard (Novopen)",Type:"M"},
  {Name:"Insugen-R",Type:"R"},
  {Name:"Insugen-N",Type:"B"},
  {Name:"Insugen-30/70",Type:"M"},
  {Name:"Novorapid",Type:"R"},
  {Name:"Novomix-30",Type:"M"},
  {Name:"Humalog",Type:"R"},
  {Name:"Humalog Mix-25",Type:"M"},
  {Name:"Humalog Mix-50",Type:"M"},
  {Name:"Levemir&reg (Detemir)",Type:"B"},
  {Name:"Lantus&reg (Glargine)",Type:"B"},
  {Name:"Insuman Rapid",Type:"R"},
  {Name:"Insuman Basal",Type:"B"},
  {Name:"Insuman Comb",Type:"M"}
]

function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const NameA = a.Name.toUpperCase();
  const NameB = b.Name.toUpperCase();

  let comparison = 0;
  if (NameA > NameB) {
    comparison = 1;
  } else if (NameA < NameB) {
    comparison = -1;
  }
  return comparison;
}

insulin = insulin.sort(compare)

insulin.unshift({Name:"None",Type:"-"})

insulin.forEach(function(d){
  $('.insulin-type').append('<option value="' + d.Type + '">' + d.Name + '</option>')
})

// Making insulin dose box appear / dissapear
$(".insulin-type").change(function(d){
  var ID = $(this).attr('id').replace('insulin-type-','');
  if ($(this).val() !== "-"){
    $('.'+ID+'Dtd').show()
  }
  else{
    $('.'+ID+'Dtd').hide()
  }
})

$(".insulin-type").trigger("change")

function clearinsulintype(){
  $(".insulin-type").each(function(d){
    var ID = $(this).attr('id').replace('insulin-type-','');
    $('.'+ID+'Dtd').hide()
  })
}

//the algorithm
var algorithms=[
  {"insulin-type-Morning":"-","insulin-type-Afternoon":"-","insulin-type-Evening":"-","insulin-type-Bed":"-",algorithm:"None"},
  {"insulin-type-Morning":"R","insulin-type-Afternoon":"R","insulin-type-Evening":"R","insulin-type-Bed":"B",algorithm:"Basal-Bolus"},
  {"insulin-type-Morning":"M","insulin-type-Afternoon":"-","insulin-type-Evening":"M","insulin-type-Bed":"-",algorithm:"Mix-BD"},
  {"insulin-type-Morning":"-","insulin-type-Afternoon":"-","insulin-type-Evening":"-","insulin-type-Bed":"B",algorithm:"Basal-OD"},
]

var algorithm = ""

function select_algorithm(){
  //check dose is complete
  var complete = true
  $(".insulin-type").each(function(d){
    var ID = $(this).attr('id').replace('insulin-type-','');
    if ($(this).val() !== "-") {
      if ($("#"+ID+"D").val() == "" || $("#"+ID+"D").val() == null) {
        $('#algorithmerror-title').html("Hmm... No dose for the selected product")
        $('#algorithmerror-body').html("You have selected a product without specifiying the dose. Looks like a mistake. Go back and enter the insulin dose again.")
        $('#algorithmerror').modal('show')
        complete = false
        return false
      }
    }
  })

  if (complete == true) {
    //screen and select algorithm
    algorithms.forEach(function(d){
      if ($('#insulin-type-Morning').val()==d['insulin-type-Morning'] &&
      $('#insulin-type-Afternoon').val()==d['insulin-type-Afternoon'] &&
      $('#insulin-type-Evening').val()==d['insulin-type-Evening'] &&
      $('#insulin-type-Bed').val()==d['insulin-type-Bed']) {
        algorithm = d['algorithm']
      }
    })
    if (algorithm == ""){
      $('#algorithmerror-title').html("Oops! The calculator is not ready... yet.")
      $('#algorithmerror-body').html("The dose calculator for this insulin regime is not yet available. Sorry about that.")
      $('#algorithmerror').modal('show')
    }
    else if (algorithm == "None") {
      $('#algorithmerror-title').html("Hmm... You did not pick any insulin.")
      $('#algorithmerror-body').html("You picked 'None' for all insulin doses. Looks like a mistake. Go back and enter the insulin dose again.")
      $('#algorithmerror').modal('show')
    }
    else{
      $('#algorithm-title').html(algorithm + " Regime Selected")
      $('#algorithm-body').html("Your insulin regimen is:<br>")
      $(".insulin-type").each(function(d){
          if($(this).val() !== "-"){
            ID = $(this).attr('id').replace('insulin-type-','')
            $('#algorithm-body').append("<b>"+ID+":</b> " + $('#'+ID+'D').val() +" units " + $('#insulin-type-'+ID+' option:selected').text() + "<br>")
          }
        })
      $('#algorithm-body').append("Are you sure?")
      $('#algorithm').modal('show')
    }
  }
}

//calculate insulin dose changes
function calculate_dose(){
  // Form validation for BG inputs
  var a = document.forms["myForm"]["BGInput1"].value;
  var b = document.forms["myForm"]["BGInput2"].value;
  var c = document.forms["myForm"]["BGInput3"].value;
  var d = document.forms["myForm"]["BGInput4"].value;
  if (a == "" && b =="" && c == "" && d == "") {
    $('#algorithmerror-title').html("Hmm.. Blood Glucose is All Blank!")
    $('#algorithmerror-body').html("You did not key in any blood glucose reading. Looks like a mistake. Go back and enter the blood glucose reading again.")
    $('#algorithmerror').modal('show')
  }
  else{
    //common variables
    var Dose_Change = [
      {Criteria:"x <=4",Change:-2},
      {Criteria:"x >=4.01 && x <=8",Change:0},
      {Criteria:"x >=8.01" ,Change:2},
    ]

    //clear all results
    var All_Result = ["MorningD-New","AfternoonD-New","EveningD-New","BedD-New"]
    All_Result.forEach(function(d){
      $("#"+d).html("")
      $("#"+d+"Prod").html("")
    })

    //The algorithm
    if (algorithm == "Basal-Bolus") {
      var Input_Output = [
        {Input:"MorningBG",Output:"BedD"},
        {Input:"AfternoonBG",Output:"MorningD"},
        {Input:"EveningBG",Output:"AfternoonD"},
        {Input:"BedBG",Output:"EveningD"}
      ]

      //get input insulin product and dose
      Input_Output.forEach(function(d){
        var x = $("#"+ d["Input"]).val();
        if (x == "") {x=6}
        var ID = d["Output"].replace("D","");
        var prod = $('#insulin-type-'+ID+' option:selected').text();
        //the actual calculation of insulin dose changes
        Dose_Change.forEach(function(e){
          if(eval(e["Criteria"])){
            var y = $("#"+ d["Output"]).val();
            var new_y = eval(y) + eval(e["Change"])
            //all <0 dose is now 0
            if(new_y < 0){
              new_y = 0;
            }
            //output the result
            $("#"+ d["Output"] + "-NewProd").html("<strong>"+prod+"</strong>")
            $("#"+ d["Output"] + "-New").html("<strong>"+ new_y + " unit</strong>")
          }
        })
      })
    }
    else if (algorithm == "Mix-BD") {
      var Input_Output = [
        {Input:["AfternoonBG","EveningBG"],Output:"MorningD"},
        {Input:["BedBG","MorningBG"],Output:"EveningD"},
      ]

      //get input values
      Input_Output.forEach(function(d){
        var array = [];
        d.Input.forEach(function(e){
          var x0 = $("#"+ e).val()
          if (x0 !== "") {
            array.push(x0)
          }
        })
        var x = Math.min(...array);
        if(x == Infinity) { x = 6; }
        var ID = d["Output"].replace("D","");
        var prod = $('#insulin-type-'+ID+' option:selected').text();
        //the actual calculation of insulin dose changes
        Dose_Change.forEach(function(e){
          if(eval(e["Criteria"])){
            var y = $("#"+ d["Output"]).val();
            var new_y = eval(y) + eval(e["Change"])
            //all <0 dose is now 0
            if(new_y < 0){
              new_y = 0;
            }
            //output the result
            $("#"+ d["Output"] + "-NewProd").html("<strong>"+prod+"</strong>")
            $("#"+ d["Output"] + "-New").html("<strong>"+ new_y + " unit</strong>")
          }
        })
      })
    }
    else if (algorithm == "Basal-OD") {
      var Input_Output = [
        {Input:["MorningBG","AfternoonBG","EveningBG","BedBG"],Output:"BedD"},
      ]

      //get input values
      Input_Output.forEach(function(d){
        var array = [];
        d.Input.forEach(function(e){
          var x0 = $("#"+ e).val()
          if (x0 !== "") {
            array.push(x0)
          }
        })
        var x = Math.min(...array);
        if(x == Infinity) { x = 6; }
        var ID = d["Output"].replace("D","");
        var prod = $('#insulin-type-'+ID+' option:selected').text();
        //the actual calculation of insulin dose changes
        Dose_Change.forEach(function(e){
          if(eval(e["Criteria"])){
            var y = $("#"+ d["Output"]).val();
            var new_y = eval(y) + eval(e["Change"])
            //all <0 dose is now 0
            if(new_y < 0){
              new_y = 0;
            }
            //output the result
            $("#"+ d["Output"] + "-NewProd").html("<strong>"+prod+"</strong>")
            $("#"+ d["Output"] + "-New").html("<strong>"+ new_y + " unit</strong>")
          }
        })
      })
    }
    $('#resultModal').modal('show')
  }
}


function toggleDose() {
    var myDose = document.getElementById('dose');
    var displaySetting = myDose.style.display;
    var doseButton = document.getElementById('doseButton');
    var myGlucose = document.getElementById('glucose');
    var displaySetting = myGlucose.style.display;
    if (displaySetting == 'block') {
      myDose.style.display = 'block';
      doseButton.innerHTML = 'Next';
      myGlucose.style.display = 'none';
      $("#doseButton").attr("onclick","select_algorithm()");
    }
    else {
      myDose.style.display = 'none';
      doseButton.innerHTML = '&ltBack to Insulin Regime';
      myGlucose.style.display = 'block';
      $("#doseButton").attr("onclick","toggleDose()");
    }
  }

function toggleResults() {
    var myResults = document.getElementById('results');
    var displaySetting = myResults.style.display;
    if (displaySetting == 'none') {
      myResults.style.display = 'block';
    }
  }

function refreshPage(){
  window.location.reload();
}
